import asyncio
import inspect
from re import sub
from sanic import Sanic, Blueprint, response
from sanic.request import Request
from sanic.response import HTTPResponse
from typing import Text, Dict, Any, Optional, Callable, Awaitable, NoReturn

import rasa.utils.endpoints
from rasa.core.channels.channel import (
    InputChannel,
    CollectingOutputChannel,
    UserMessage,
)
import requests
import pymongo
from actions import config
import datetime
import urllib


"""
Sample message payload 
{
  "app_id": "4265695042642543529",
  "user_id_by_app": "1550109092896967015",
  "event_name": "user_send_text",
  "timestamp": "1623661598834",
  "sender": {
    "id": "3470834374893874255"
  },
  "recipient": {
    "id": "579745863508352884"
  },
  "message": {
    "msg_id": "This is message id",
    "text": "This is testing message"
  }
}
"""


class ZaloIO(InputChannel):
    def __init__(self, *args, **kwargs) -> None:
        super(InputChannel, self).__init__(*args, **kwargs)
        self.mongo_client = pymongo.MongoClient(config.mongodb_connection_string)
        self.mongo_db = self.mongo_client[config.mongodb_db_name]

    def name(self) -> Text:
        """Name of your custom channel."""
        return "zalo"

    def get_metadata(self, request: Request) -> Optional[Dict[Text, Any]]:
        # store sender id into the database
        # TODO: needs redis to fast check if user already exists. Right now
        # just use mongodb for this check also
        try:  # test message from webhook setup, ignore
            sender_id = request.json.get("sender").get("id")  # method to get sender_id
        except:
            return None
        users_col = self.mongo_db[config.USER_COLLECTION]
        user = users_col.find_one({"user_id": sender_id})
        if user:
            del user["_id"]
            user["birthday"] = (
                user["birthday"].strftime("%d/%m/%Y") if user["birthday"] else None
            )
        else:
            print(
                f"-- user {sender_id} not found in collection {config.USER_COLLECTION}, inserting"
            )
            print(f"-- query zalo for user information")
            zalo_res = requests.get(
                "https://openapi.zalo.me/v2.0/oa/getprofile",
                params={
                    "access_token": config.ZALO_ACCESS_TOKEN,
                    "data": '{"user_id": "' + str(sender_id) + '"}',
                },
            ).json()
            print("--respose from zalo", zalo_res)
            gender = zalo_res["data"]["user_gender"]
            subject = "anh" if gender == 1 else "chị"
            display_name = zalo_res["data"]["display_name"]
            name = display_name.split()[-1].title()
            try:
                birthday = zalo_res["data"]["birth_date"]
                birthday = datetime.datetime.fromtimestamp(int(birthday))
            except Exception as e:
                print("--error converting birthday", e)
                birthday = None
            user = {
                "user_id": sender_id,
                "name": name,
                "subject": subject,
                "display_name": display_name,
                "birthday": birthday,
            }
            users_col.insert_one(user)
            user["birthday"] = (birthday.strftime("%d/%m/%Y") if birthday else None,)
            del user["_id"]
            print(f"-- inserted user {user}")
        return user

    def blueprint(
        self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:

        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        @custom_webhook.route("/", methods=["GET", "POST"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({"status": "ok"})

        @custom_webhook.route("/webhook", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            try:  # test message from webhook setup, ignore
                sender_id = request.json.get("sender").get(
                    "id"
                )  # method to get sender_id
            except:
                sender_id = ""

            try:  # test message from webhook setup, ignore
                text = request.json.get("message").get("text")  # method to fetch text
            except:
                text = ""
            try:
                msg_id = request.json.get("message").get("msg_id")
            except:
                msg_id = ""

            input_channel = self.name()  # method to fetch input channel
            metadata = self.get_metadata(request)  # method to get metadata
            print(f"sender_id = {sender_id}, text = {text}, msg_id = {msg_id}")

            collector = CollectingOutputChannel()

            # include exception handling

            await on_new_message(
                UserMessage(
                    text,
                    collector,
                    sender_id,
                    input_channel=input_channel,
                    metadata=metadata,
                )
            )

            response_messages = collector.messages
            for res in response_messages:
                print("response keys", res.keys())
                print("response", res)
                if "text" in res:
                    zalo_res = requests.post(
                        f"https://openapi.zalo.me/v2.0/oa/message?access_token={config.ZALO_ACCESS_TOKEN}",
                        json={
                            "recipient": {"message_id": f"{msg_id}"},
                            "message": {"text": res["text"]},
                        },
                        verify=False,
                    )
                elif "image" in res:
                    zalo_res = requests.post(
                        f"https://openapi.zalo.me/v2.0/oa/message?access_token={config.ZALO_ACCESS_TOKEN}",
                        json={
                            "recipient": {"message_id": f"{msg_id}"},
                            "message": {
                                "attachment": {
                                    "payload": {
                                        "elements": [
                                            {
                                                "media_type": "image",
                                                "url": res["image"],
                                            }
                                        ],
                                        "template_type": "media",
                                    },
                                    "type": "template",
                                },
                                "text": "",
                            },
                        },
                        verify=False,
                    )
                print("status from zalo", zalo_res.status_code)
                print("response from zalo", zalo_res.text)

            return response.json(collector.messages)

        return custom_webhook
