import json
import pytest

import os
from pathlib import Path
import pytest
import json

from rasa_sdk import Action, Tracker
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    EventType,
)

from actions.action_stocktraders import *


@pytest.fixture
def dispatcher():
    return CollectingDispatcher()


@pytest.fixture
def domain():
    return dict()


here = Path(__file__).parent.resolve()


def init_empty_tracker():
    return Tracker.from_dict(json.load(open(here / "./empty_tracker.json")))


@pytest.mark.asyncio
async def test_action_ask_ticker_price(dispatcher, domain):
    tracker = init_empty_tracker()
    ticker = "ACB"
    tracker.slots["portfolio"] = [ticker]
    action = ActionAskUnprocessedTicker()
    events = action.run(dispatcher, tracker, domain)
    print(events)
    assert events == [
        SlotSet("processed_tickers", [ticker]),
        SlotSet("ticker", ticker),
        FollowupAction("utter_ask_past_buy_price"),
    ]


SAMPLE_GET_TICKER_INFO_REPLY = {
    "TickerInfoReply": {
        "codeReply": {"codeID": "", "codeName": ""},
        "ave": 50.45,
        "close": "50.70",
        "ticker": "TCB",
        "tickerInfos": {
            "date": "2021-05-04",
            "percent": "20%",
            "price": 42.06,
            "trade": "MUA",
            "urlImg": "http://stocktraders.vn/userfiles/image/stockshare/2021/5/4/3/48/746e2d24ef7e1a20436f.jpg",
        },
        "trade": "NẮM GIỮ",
    }
}


def test_action_get_past_buy_price_when_lower(dispatcher, domain):
    tracker = init_empty_tracker()
    tracker.slots["price"] = 45
    tracker.slots["portfolio"] = ["ACB"]
    res = SAMPLE_GET_TICKER_INFO_REPLY
    action = ActionGetPastBuyPrice()
    events = action.run_from_result(dispatcher, tracker, domain, "ACB", res)
    assert events == [
        SlotSet("past_buy_price", 42.06),
        SlotSet("past_buy_date", "2021-05-04"),
        SlotSet("processed_tickers", ["ACB"]),
        FollowupAction("utter_inform_past_buy_price_better"),
    ]


def test_action_get_past_buy_price_when_higher(dispatcher, domain):
    tracker = init_empty_tracker()
    tracker.slots["price"] = 30
    tracker.slots["portfolio"] = ["ACB"]
    res = SAMPLE_GET_TICKER_INFO_REPLY
    action = ActionGetPastBuyPrice()
    events = action.run_from_result(dispatcher, tracker, domain, "ACB", res)
    assert events == [
        SlotSet("processed_tickers", ["ACB"]),
        FollowupAction("utter_inform_past_app_buy_price_worse"),
    ]
