import logging
from typing import Dict, List, Text, Any
from rasa_sdk import Action, Tracker
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    EventType,
    FollowupAction,
)
import json
import requests
from pprint import pprint
from datetime import date, datetime
import pathlib
import os

ticker_file = str(
    pathlib.Path(__file__).parent.absolute().parent / "data/lookups/lookup_tickers.txt"
)
with open(ticker_file) as file:
    ticker_list = file.readlines()
    ticker_list = [i.strip() for i in ticker_list]
print(f"-- len of ticker list = {len(ticker_list)}")


class ActionValidateAdminForm:
    def _validate_date(self, slot_value):
        try:
            date = datetime.strptime(slot_value, "%d%m%y").strftime("%Y-%m-%d")
            return {"date": slot_value}
        except:
            return {"date": None}

    def _validate_condition(self, slot_value):
        try:
            return {"condition": int(slot_value)}
        except:
            return {"condition": None}

    def _validate_ticker(self, slot_value):
        if slot_value and slot_value.upper() in ticker_list:
            return {"ticker": slot_value}
        else:
            return {"ticker": None}


class ActionValidate_form_admin_ask_condition_with_date(
    FormValidationAction, ActionValidateAdminForm
):
    def name(self) -> Text:
        return "validate_form_admin_ask_condition_with_date"

    def validate_date(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        return self._validate_date(slot_value=slot_value)

    def validate_condition(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        return self._validate_condition(slot_value=slot_value)


class ActionValidate_form_admin_ask_ticker_with_date(
    FormValidationAction, ActionValidateAdminForm
):
    def name(self) -> Text:
        return "validate_form_admin_ask_ticker_with_date"

    def validate_date(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        return self._validate_date(slot_value=slot_value)

    def validate_ticker(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        return self._validate_ticker(slot_value=slot_value)
