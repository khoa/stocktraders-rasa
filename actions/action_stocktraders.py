import logging
from typing import Dict, List, Text
from rasa_sdk import Action, Tracker
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    EventType,
    FollowupAction,
)
import json
import requests
from pprint import pprint
from datetime import date, datetime


logger = logging.getLogger(__name__)


class ApiStockTraders:
    def __init__(self) -> None:
        pass

    def get_ticker_info(ticker):
        res = requests.post(
            "https://stocktraders.vn/service/api/GetTickerInfo",
            json={"TickerInfoRequest": {"ticker": ticker}},
        )
        res = res.json()
        ticker_infos = res["TickerInfoReply"]["tickerInfos"]
        if type(ticker_infos) != list:  # make a list
            ticker_infos = [ticker_infos]
        for i in range(len(ticker_infos)):
            ticker_infos[i]["price"] = float(ticker_infos[i]["price"])
        close = float(res["TickerInfoReply"]["close"])
        return ticker_infos, close

    def get_indicator_info(ticker, date, condition):
        print(f"ticker = {ticker}, date = {date}, condition = {condition}")
        payload = {
            "TickerCondRequest": {
                "ticker": ticker.upper() if ticker else None,
                "date": date,
                "cond": condition,
            }
        }
        res = requests.post(
            "https://stocktraders.vn/service/api/GetTickerCond",
            json=payload,
        )
        print("-- TickerCondRequest response", res.text)

        return res.json()

    def get_all_conditions():
        res = requests.get("https://stocktraders.vn/service/api/GetCondition")
        print("-- response from get all conditions", res.text)
        return res.json()["ConditionReply"]["conds"]


class ActionGetAllConditions(Action):
    def name(self) -> Text:
        return "action_get_all_conditions"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> List[EventType]:
        res = ApiStockTraders.get_all_conditions()
        return [SlotSet("all_conditions", "\n".join(res))]


class ActionGetMarketOutlook(Action):
    def name(self) -> Text:
        return "action_get_market_outlook"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> List[EventType]:
        """Once we have an email, attempt to add it to the database"""

        # email = tracker.get_slot("email")
        # client = MailChimpAPI(config.mailchimp_api_key)
        # subscription_status = client.subscribe_user(config.mailchimp_list, email)

        # if subscription_status == "newly_subscribed":
        #     dispatcher.utter_message(template="utter_confirmationemail")
        # elif subscription_status == "already_subscribed":
        #     dispatcher.utter_message(template="utter_already_subscribed")
        # elif subscription_status == "error":
        #     dispatcher.utter_message(template="utter_could_not_subscribe")
        # return []

        res = requests.get("https://stocktraders.vn/service/api/GetMarket")
        res = res.json()
        info = res["MarketReply"]["markets"]
        # sometimes the result is only 1 string
        if type(info) == str:
            content = info
        else:
            content = ", ".join(res["MarketReply"]["markets"])
        dispatcher.utter_message(text=content)
        return []


class ActionSubmitIndicatorForm(Action):
    def name(self) -> Text:
        return "action_submit_indicator_form"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> List[EventType]:
        print("-- submitting indicator form")
        date = tracker.get_slot("date")
        try:
            date = datetime.strptime(date, "%d%m%y").strftime("%Y-%m-%d")
        except:
            print(
                f"-- failed to parse date from user input {date}, probably user enter the wrong thing. Ask for date again"
            )
            return [SlotSet("date", None)]
        ticker = tracker.get_slot("ticker")
        if not ticker:
            ticker = ""
        condition = tracker.get_slot("condition")
        if not condition:
            condition = 0
        res = ApiStockTraders.get_indicator_info(
            ticker=ticker, date=date, condition=condition
        )
        if "values" in res["TickerCondReply"]:
            result = res["TickerCondReply"]["values"]
            if type(result) == list:
                result = " ".join(sorted(result))
        else:
            result = "Không có điều kiện nào"
        print("-- result", result)
        if not ticker:
            dispatcher.utter_message(
                text=f"Kết quả tra cứu condition {condition} vào ngày {date}: {result}"
            )
        else:
            dispatcher.utter_message(
                text=f"Kết quả tra cứu ngày {date} cho {ticker.upper()}: {result}"
            )

        return [
            SlotSet("date", None),
            SlotSet("ticker", None),
            SlotSet("condition", None),
            SlotSet("all_conditions", None),
        ]


class ActionAskUnprocessedTicker(Action):
    def name(self) -> Text:
        return "action_ask_unprocessed_ticker"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> List[EventType]:
        portfolio = tracker.get_slot("portfolio")
        portfolio = [i.upper() for i in portfolio]
        if len(portfolio) == 1:  # kh only has 1 ticker
            ticker = portfolio[0]
            print(f"-- only have 1 {ticker}, asking for past buy price")
            return [
                SlotSet("ticker", ticker),
                FollowupAction("utter_ask_past_buy_price"),
            ]
        else:  # call api to determine which ticker has current price > app's price, show
            for ticker in portfolio:
                ticker_infos, close = ApiStockTraders.get_ticker_info(ticker)
                buy_prices = [t["price"] for t in ticker_infos if t["trade"] == "MUA"]
                if len(buy_prices) > 0 and min(buy_prices) < close:
                    print(
                        f"-- ticker {ticker} has buy price lower than current price, asking for customer price"
                    )
                    return [
                        SlotSet("ticker", ticker),
                        FollowupAction("utter_ask_past_buy_price"),
                    ]
            print("none of the tickers in portfolio satisfy condition")
        return []


class ActionGetPastBuyPrice(Action):
    def name(self) -> Text:
        return "action_get_past_buy_price"

    def run_from_result(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
        ticker: str,
        ticker_infos: List,
    ) -> List[EventType]:
        print("-- ticker info response", ticker_infos)
        customer_past_buy_price = float(tracker.get_slot("price"))
        # only inform about the better price when the app's price is better than customer's
        buy_signals = [
            i
            for i in ticker_infos
            if i["trade"] == "MUA" and float(i["price"]) < customer_past_buy_price
        ]
        if buy_signals:
            past_buy_price = float(buy_signals[-1]["price"])
            past_buy_date = buy_signals[-1]["date"]
            image_url = buy_signals[-1]["urlImg"]

            print(f"ticker {ticker} could be bought at a better price {past_buy_price}")
            dispatcher.utter_message(image=image_url)
            return [
                SlotSet("past_buy_price", past_buy_price),
                SlotSet("past_buy_date", past_buy_date),
                FollowupAction("utter_inform_past_buy_price_better"),
            ]

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> List[EventType]:
        ticker = tracker.get_slot("ticker")
        print(f"--asking for past buy price for {ticker}")
        ts = datetime.now()
        ticker_infos, close = ApiStockTraders.get_ticker_info(ticker)
        print(f"request ticker info taken = {datetime.now() - ts}")
        return self.run_from_result(
            dispatcher, tracker, domain, ticker, ticker_infos=ticker_infos
        )
