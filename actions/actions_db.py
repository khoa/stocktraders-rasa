import pymongo
import logging
from typing import Dict, List, Text
from rasa_sdk import Action, Tracker
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    EventType,
    FollowupAction,
)
import json
import requests
from pprint import pprint
from datetime import date, datetime
from actions import config


class ActionLoadUserInfo(Action):
    def name(self) -> Text:
        return "action_load_user_info"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> List[EventType]:
        print("-- running action load user info")
        user_events = [e for e in tracker.events if e["event"] == "user"]
        try:
            user_metadata = user_events[-1]["metadata"]
            if user_metadata:
                subject = user_metadata["subject"] + " " + user_metadata["name"]
                return [SlotSet("subject", subject)]

        except Exception as e:
            print("error getting user metadata", e)

        return []
