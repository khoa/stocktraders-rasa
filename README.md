
```
pip install rasa[full]
pip install pyvi
https://gitlab.com/trungtv/vi_spacy/-/raw/master/vi_core_news_lg/dist/vi_core_news_lg-0.0.1.tar.gz
```

To train the model: 
```
rasa train
```

To run the demo 
```
# start duckling server
docker run -p 8011:8000 rasa/duckling

# start mongodb 
mongod --dbpath ./mongodb-data

# run action server 
rasa run actions

# run rasa server 
rasa run -p 5050 --endpoints endpoints.yml --credentials credentials.yml --debug
```