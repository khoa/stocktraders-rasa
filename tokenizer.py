from unidecode import unidecode
from rasa.nlu.tokenizers.tokenizer import Token, Tokenizer
from rasa.nlu.constants import TOKENS_NAMES, MESSAGE_ATTRIBUTES
from underthesea import word_tokenize
from typing import Any, Dict, List, Text


class VietnameseTokenizer(Tokenizer):
    provides = [TOKENS_NAMES[attribute] for attribute in MESSAGE_ATTRIBUTES]

    def __init__(self, component_config: Dict[Text, Any] = None) -> None:
        super().__init__(component_config)

    def tokenize(self, message, attribute: Text) -> List[Token]:
        text = message.get(attribute)
        text = unidecode(text)
        words = word_tokenize(text)

        return self._convert_words_to_tokens(words, text)
